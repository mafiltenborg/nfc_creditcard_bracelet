translate([50,0,0]){
    rotate([90,0,0]){
        difference(){
            cube([88,56.5,3.2]);
            translate([1,1,1]){
                cube([86,54.5,1.2]);
            }
            translate([10,10,1.5]){
                cube([68,36.5,3]);
            }
            translate([82,-1,2.1]){
                cube([10,58,2]);
            }
            translate([88,27.75,-1]){
                cylinder(d=20, h=3.15);
            }
        }
    }
}

difference(){
    cube([40,30,4]);
    translate([8.5,-1,1.5]){
        cube([23,34,3]);
    }
}